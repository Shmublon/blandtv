import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('Header text test', () => {
  render(<App />);
  const headerText = screen.getByText(/tv show and web series database./i);
  expect(headerText).toBeInTheDocument();
});
