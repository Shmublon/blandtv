import React, {useEffect, useState} from 'react';
import {Container, Row, Col} from 'react-bootstrap';
import Intro from '../components/Intro/Intro';
import ShowItem from '../components/Items/ShowItem';
import ShowInfo from '../components/ShowInfo/ShowInfo';
import StarringInfo from '../components/Starring/StarringInfo';
import {useParams} from "react-router-dom";
import {ICast, IShow} from "../api/interfaces";
import {fetchShowCast, fetchSingleShow} from "../api/showsApi";

const Show: React.FC = () => {
  const {showId} = useParams<{ showId: string }>();
  const [show, setShow] = useState<IShow>();
  const [starring, setStarring] = useState<ICast[]>([]);

  useEffect(() => {
    const loadShowDetails = async () => {
      try {
        setShow(await fetchSingleShow(showId))
        setStarring(await fetchShowCast(showId))
      } catch (e) {
        alert('Something went wrong!!')
      }
    }
    loadShowDetails()
  }, [showId])

  return (
    <div className="show">
      <Intro>
        <ShowItem
          show={show}
        />
      </Intro>
      <div className="content">
        <Container>
          <Row>
            <Col xs={12} md={6}>
              <ShowInfo show={show}/>
            </Col>
            <Col xs={12} md={6}>
              <StarringInfo starring={starring.slice(0, 4)}/>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  )
}

export default Show;
