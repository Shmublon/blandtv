import React, {useEffect, useState} from 'react';
import EpisodesGrid from '../components/GridShows/EpisodesGrid';
import Intro from '../components/Intro/Intro';
import LeadText from '../components/LeadText/LeadText';
import {Container} from 'react-bootstrap';
import {IEpisode} from "../api/interfaces";
import {fetchLastEpisodes} from "../api/showsApi";

const Home: React.FC = () => {
  const [episodes, setEpisodes] = useState<IEpisode[]>(new Array(18).fill(null));
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    const loadEpisodes = async () => {
      try {
        setEpisodes(await fetchLastEpisodes())
        setLoading(false);
      } catch (e) {
        setLoading(false);
        alert('Something went wrong!!')
      }
    }
    loadEpisodes()
  }, [])

  return (
    <div className="home-page">
      <Intro>
        <LeadText
          text={[
            "TV Show and web series database.",
            <br/>,
            "Create personalised schedules. Episode guide, cast, crew and character information."
          ]}/>
      </Intro>
      <div className="content">
        <Container>
          <EpisodesGrid
            isLoading={isLoading}
            episodes={episodes}
          />
        </Container>
      </div>
    </div>
  )
}

export default Home;

