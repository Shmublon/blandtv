import {ICast, IEpisode, IShow} from "./interfaces";

const API = "https://api.tvmaze.com/"

export const fetchLastEpisodes = async (): Promise<IEpisode[]> => {
  const episodesResponse = await fetch(`${API}schedule`);
  return (await episodesResponse.json()) as IEpisode[];
};

export const fetchSingleShow = async (showId: string): Promise<IShow> => {
  const showResponse = await fetch(`${API}shows/${showId}`);
  return (await showResponse.json()) as IShow;
};

export const fetchShowCast = async (showId: string): Promise<ICast[]> => {
  const castResponse = await fetch(`${API}shows/${showId}/cast`);
  return (await castResponse.json()) as ICast[];
};
