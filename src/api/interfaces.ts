export interface IPerson {
  id: number,
  url: string,
  name: string,
  country: ICountry,
  birthday: string,
  deathday?: string,
  gender: string,
  image?: ICoverImage,
  updated: number,
  _links: {
    self: {
      href: string
    }
  }
}

export interface ICharacter {
  id: number,
  url: string,
  name: string,
  image?: ICoverImage,
  _links: {
    self: {
      href: string
    }
  }
}

export interface ICast {
  person: IPerson
  character: ICharacter,
  self: boolean,
  voice: boolean,
}

export interface IShow {
  id: number,
  url: string,
  name: string,
  type: string,
  language: string,
  genres: string[],
  status: string,
  runtime: number,
  averageRuntime: number,
  premiered: string,
  ended?: string,
  officialSite?: string,
  schedule: {
    time: string,
    days: string[]
  },
  rating: {
    average?: number
  },
  weight: number,
  network: INetwork,
  webChannel: null,
  dvdCountry: null,
  externals: {
    tvrage?: number,
    thetvdb?: number,
    imdb?: number
  },
  image?: ICoverImage,
  summary: string,
  updated: number,
  _links: {
    self: {
      href: string
    },
    previousepisode: {
      href: string
    },
    nextepisode: {
      href: string
    }
  }
}

export interface IEpisode {
  id: number,
  url: string,
  name: string,
  season: number,
  number: number,
  type: string,
  airdate: string,
  airtime: string,
  airstamp: string,
  runtime: number,
  image?: ICoverImage,
  summary?: string,
  show: IShow,
  _links: {
    self: {
      href: string
    }
  }
}

export interface INetwork {
  id: number,
  name: string,
  country: ICountry
}

export interface ICountry {
  name: string,
  code: string,
  timezone: string
}

export interface ICoverImage {
  medium: string,
  original: string
}
