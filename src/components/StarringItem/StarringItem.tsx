import React from 'react';
import { Link } from "react-router-dom";

interface Props {
  photoUrl?: string,
  photoAlt: string,
  name: string,
  characterName: string,
}

const StarringItem: React.FC<Props> = (props) => {
  return  (
    <div className="starring-item">
      <Link to="#" className="img-holder">
        {props.photoUrl && <img className="img-fluid" src={props.photoUrl} alt={props.photoAlt} />}
      </Link>
      <div className="text-holder">
        <h4 className="h4 title"><Link to="#">{props.name}</Link></h4>
        <p>{props.characterName}</p>
      </div>
    </div>
  )
}

export default StarringItem;

