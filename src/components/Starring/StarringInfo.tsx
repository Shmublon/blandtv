import React from 'react';

import {Row, Col} from 'react-bootstrap';
import StarringItem from '../../components/StarringItem/StarringItem';
import {ICast} from "../../api/interfaces";

const StarringInfo: React.FC<{ starring?: ICast[] }> = ({starring}) => {
  return (
    <div>
      <h3 className="h3">Starring</h3>
      <Row>
        {starring && starring.map(star => {
          return <Col xs={12} key={star.person.id}>
            <StarringItem
              photoAlt="Star photo"
              photoUrl={star.person.image ? star.person.image.medium : undefined}
              name={star.person.name}
              characterName={star.character.name}
            />
          </Col>
        })}
      </Row>
    </div>
  )
}

export default StarringInfo;

