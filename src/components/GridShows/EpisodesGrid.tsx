import React from 'react';
import {Row} from 'react-bootstrap';

import {IEpisode} from "../../api/interfaces";
import EpisodeItem from "../Items/EpisodeItem";

interface Props {
  episodes: IEpisode[],
  isLoading: boolean,
}

const EpisodesGrid: React.FC<Props> = ({episodes, isLoading}) => {
  return (
    <div>
      <h1 className="h1">Last Added Shows</h1>
      <Row>
        {episodes.map((episode, index) => {
          return <EpisodeItem
              isLoading={isLoading}
              episode={episode}
            />
        })}
      </Row>
    </div>
  )
}

export default EpisodesGrid;

