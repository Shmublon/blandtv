import React from 'react';

import { Row, Col } from 'react-bootstrap';
import ShowInfoItem from '../../components/ShowInfoItem/ShowInfoItem';
import {IShow} from "../../api/interfaces";

const ShowInfo: React.FC<{show?: IShow}> = ({show}) => {
  return  (
    <div className="show-info">
        <h3 className="h3">Show Info</h3>
        <Row>
            <Col xs={6} md={12}>
                <ShowInfoItem
                    title="Streamed on"
                    text={show?.network.name}
                    />
            </Col>
            <Col xs={6} md={12}>
                <ShowInfoItem
                    title="Schedule"
                    text={show?.schedule.days.join(', ')}
                    />
            </Col>
            <Col xs={6} md={12}>
                <ShowInfoItem
                    title="Status"
                    text={show?.status}
                    />
            </Col>
            <Col xs={6} md={12}>
                <ShowInfoItem
                    title="Genres"
                    text={show?.genres.join(', ')}
                    />
            </Col>
        </Row>
    </div>
  )
}

export default ShowInfo;

