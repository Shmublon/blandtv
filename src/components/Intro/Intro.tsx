import React from 'react';
import { Container } from 'react-bootstrap';

const Intro: React.FC = ( {children} ) => {
  return  (
    <div className="intro">
      <Container>
        {children}
      </Container>
    </div>
  )
}

export default Intro;
