import React, {ReactElement} from 'react';

interface Props {
  text: (string | ReactElement)[],
}

const LeadText: React.FC<Props> = (props) => {
  return  (
    <p className="lead-text">{props.text}</p>
  )
}

export default LeadText;
