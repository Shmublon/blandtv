import React from 'react';
import { Container } from 'react-bootstrap';
import { Link } from "react-router-dom";

import logo from '../../images/logo.svg';

const Header: React.FC = () => {
  return  (
    <header>
      <Container>
        <Link to="/" className="logo" href="#"><img src={logo} alt="TV Bland" /></Link>
      </Container>
    </header>
  )
}

export default Header;
