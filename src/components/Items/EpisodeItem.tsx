import React from 'react';

import {Rating, Skeleton} from "@mui/material";
import {Link} from "react-router-dom";
import {IEpisode} from "../../api/interfaces";
import {Col} from "react-bootstrap";

interface Props {
  isLoading: boolean,
  episode?: IEpisode,
}


const EpisodeItem: React.FC<Props> = ({episode, isLoading}) => {
  if (isLoading || !episode) {
    return (
      <Col xs={6} sm={4} md={3} lg={2}>
        <div className="show-item show-item-grid">
          <div className="img-holder">
            {!isLoading && episode?.show.image &&
            <img className="img-fluid" src={episode.show.image.medium} alt="Episode cover"/>}
          </div>
          <div className="text-holder">
            <Skeleton className="rating" height={24} width={120}/>
            <Skeleton className="h2 title" height={24} width={"100%"}/>
          </div>
        </div>
      </Col>
    )
  }

  return (
    <Col xs={6} sm={4} md={3} lg={2}>
      <Link to={`/show/${episode?.show.id}`} className="show-item-link">
        <div className="show-item show-item-grid">
          <div className="img-holder">
            {!isLoading && episode.show.image &&
            <img className="img-fluid" src={episode.show.image.medium} alt="Episode cover"/>}
          </div>
          <div className="text-holder">
            <div className="rating">
              <Rating
                emptyLabelText="N/A"
                precision={0.2}
                value={episode?.show.rating.average ? episode?.show.rating.average / 2 : episode?.show.rating.average}
                readOnly/>
            </div>
            <h2 className="h2 title">{episode?.name}</h2>
          </div>
        </div>
      </Link>
    </Col>
  )
}

export default EpisodeItem;

