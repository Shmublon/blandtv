import React, {useState} from 'react';

import {IShow} from "../../api/interfaces";
import ReactHtmlParser from 'react-html-parser';
import {Rating, Skeleton} from "@mui/material";

const ShowItem: React.FC<{ show?: IShow }> = ({show}) => {
  const [lessMore, setLessMore] = useState("description");

  if (show) {
    const ratingValue = show.rating.average ? show.rating.average / 2 : show.rating.average;
    return (
      <div className="show-item show-item-preview">
        <div className="img-holder">
          {show.image && <img className="img-fluid" src={show.image?.original} alt="Show cover"/>}
        </div>
        <div className="text-holder">
          <div className="rating">
            <Rating
              name="read-only"
              value={ratingValue}
              precision={0.2}
              readOnly/>
            <strong>{ratingValue ? `${ratingValue}/5` : 'N/A'}</strong>
          </div>
          <h2 className="h2 title">{show.name}</h2>
          <div className={lessMore}>
            {ReactHtmlParser(show.summary)}
          </div>
          <span className="show-less-more" onClick={() => setLessMore(lessMore ? "" : "description")}>Show {lessMore ? "more" : "less"}</span>
        </div>
      </div>
    )
  } else {
    return <div className="show-item show-item-preview">
      <div className="img-holder" />
      <div className="text-holder">
        <div className="rating">
          <Skeleton className="rating" height={24} width={120}/>
        </div>
        <Skeleton className="h2 title" height={55} width={"100%"}/>
        <Skeleton className="h2 title" height={120} width={"100%"}/>
      </div>
    </div>
  }
}

export default ShowItem;

