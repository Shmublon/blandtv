import React from 'react';

interface Props {
  title: string,
  text?: string,
}

const ShowInfoItem: React.FC<Props> = (props) => {
  return  (
    <div className="show-info-item">
      <h4 className="h4 title">{props.title}</h4>
      <p>{props.text}</p>
    </div>
  )
}

export default ShowInfoItem;

