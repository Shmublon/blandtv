import React from 'react';
import {Route, BrowserRouter, Switch} from "react-router-dom";

import Header from './components/Header/Header';
import Home from './pages/Home';
import Show from './pages/Show';

const App: React.FC = () => {
  return (
    <BrowserRouter>
      <div id="wrapper">
        <Header/>
        <Switch>
          <Route path="/show/:showId">
            <Show/>
          </Route>
          <Route path="/">
            <Home/>
          </Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
